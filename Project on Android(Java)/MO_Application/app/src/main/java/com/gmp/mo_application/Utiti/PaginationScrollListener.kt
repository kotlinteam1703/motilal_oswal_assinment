package com.gmp.mo_application.Utiti

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract  class PaginationScrollListener(val layoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

     var layoutManagerVar : LinearLayoutManager
         get() = layoutManager
         set(value) = TODO()

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManagerVar.childCount
        val totalItemCount = layoutManagerVar.itemCount
        val firstVisibleItemPosition = layoutManagerVar.findFirstVisibleItemPosition()

        if (!isLoading() && !isLastPage()) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
            ) {
                loadMoreItems()
            }
        }
    }

    protected abstract fun loadMoreItems();

     abstract fun isLastPage(): Boolean;

     abstract fun isLoading():Boolean;
}