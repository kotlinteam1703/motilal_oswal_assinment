package com.gmp.mo_application.views.activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gmp.mo_application.R
import com.gmp.mo_application.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding :ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}