package com.gmp.mo_application.views.activites.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.lifecycle.ViewModelProvider
import com.gmp.mo_application.R
import com.gmp.mo_application.apis.RepoService
import com.gmp.mo_application.constants.RetrofitHelper
import com.gmp.mo_application.repositorys.Repo_repository
import com.gmp.mo_application.viewModel.RepoListViewModel
import com.gmp.mo_application.viewModel.RepoListViewModelFactory
import kotlinx.android.synthetic.main.fragment_repo_list.view.*
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import com.gmp.mo_application.Utiti.PaginationScrollListener

import com.gmp.mo_application.adaptor.RepoAdapor
import com.gmp.mo_application.models.Repo
import kotlin.emptyArray as emptyArray1


class RepoListFragment : Fragment() {

    private lateinit var repoListViewModel: RepoListViewModel
    var layoutManager: LinearLayoutManager? = null
    var isLastPage: Boolean = false
    var isLoading: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_repo_list, container, false)
        inItViewModel()

        val recler=view.findViewById<RecyclerView>(R.id.recycler)
        layoutManager = LinearLayoutManager(activity)
        recler.setLayoutManager(layoutManager)
        val mutableList : MutableList<Repo> = mutableListOf()
        var adapter = RepoAdapor(mutableList )

        recler.adapter =  adapter
        repoListViewModel.repoList.observe(viewLifecycleOwner, {

            adapter.onRefresh(it as MutableList<Repo>)
            view.progress.visibility = View.GONE
        }
        )

        recler.addOnScrollListener(object : PaginationScrollListener(layoutManager!!){
            override fun loadMoreItems() {
                view.progress.visibility = View.VISIBLE
                isLoading = true

                //you have to call loadmore items to get more data
                repoListViewModel.getRepoList()
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLastPage
            }

        })

        return view
    }


    private fun inItViewModel() {
        val repoService = RetrofitHelper.provideRetrofit().create(RepoService::class.java)
        val repo_repository = Repo_repository(repoService)
        repoListViewModel = ViewModelProvider(
            this,
            RepoListViewModelFactory(repo_repository)
        ).get(RepoListViewModel::class.java)
    }


}