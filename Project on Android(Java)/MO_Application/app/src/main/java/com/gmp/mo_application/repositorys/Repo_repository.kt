package com.gmp.mo_application.repositorys

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gmp.mo_application.apis.RepoService
import com.gmp.mo_application.models.Repo

class Repo_repository(private val repoService: RepoService) {

    private val repoLiveData = MutableLiveData<List<Repo>>()
    val arrayList = ArrayList<Repo>()

     val repoLive : LiveData<List<Repo>> get()=repoLiveData


    suspend fun getRepoList(  query :String, page : Int, itemsPerPage : Int ){

        val result=repoService.getTrendingRepos(query,page,itemsPerPage)
        arrayList.addAll(result.items)

        repoLiveData.postValue(arrayList)

    }
}