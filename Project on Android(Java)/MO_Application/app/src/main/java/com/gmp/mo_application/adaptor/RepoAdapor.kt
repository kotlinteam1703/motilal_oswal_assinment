package com.gmp.mo_application.adaptor

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gmp.mo_application.databinding.ItemRepoBinding
import com.gmp.mo_application.models.Repo
import com.gmp.mo_application.views.activites.fragments.RepoListFragmentDirections

class RepoAdapor(var  listData: MutableList<Repo>) :
    RecyclerView.Adapter<RepoAdapor.RepoViewHolder>() {
    lateinit var bindind: ItemRepoBinding

    fun onRefresh(listData: MutableList<Repo>) {
        this.listData = listData
        notifyDataSetChanged()
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        bindind = ItemRepoBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return RepoViewHolder(bindind)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {

        holder.bind(createOnClickListener(bindind, listData[position]), listData[position])
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    private fun createOnClickListener(binding: ItemRepoBinding, repo: Repo): View.OnClickListener {
        return View.OnClickListener {
            val directions = RepoListFragmentDirections.actionRepoDetails(repo)
            val extras = FragmentNavigatorExtras(
                binding.avatar to "avatar_${repo.id}"
            )
            it.findNavController().navigate(directions, extras)
        }
    }

    inner class RepoViewHolder(private val binding: ItemRepoBinding) :
        RecyclerView.ViewHolder(binding.root) {
           fun bind(listener: View.OnClickListener, repo: Repo) {

            binding.apply {

                Glide.with(itemView)
                    .load(repo.owner.avatar_url)
                    .centerCrop()
                    .error(android.R.drawable.stat_notify_error)
                    .into(avatar)

                val str = SpannableString(repo.owner.login + " / " + repo.name)
                str.setSpan(
                    StyleSpan(Typeface.BOLD),
                    repo.owner.login.length,
                    str.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                name.text = str

                description.text = repo.description

                language.text = repo.language

                ViewCompat.setTransitionName(this.avatar, "avatar_${repo.id}")

                root.setOnClickListener(listener)
            }

        }

    }

}