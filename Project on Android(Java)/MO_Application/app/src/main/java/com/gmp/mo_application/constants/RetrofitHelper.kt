package com.gmp.mo_application.constants

import com.gmp.mo_application.apis.RepoService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {

    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(RepoService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}