package com.gmp.mo_application.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmp.mo_application.models.Repo
import com.gmp.mo_application.repositorys.Repo_repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class RepoListViewModel(val repository :Repo_repository) : ViewModel() {

    var page: Int =0
    var itemPerPage: Int =20


    init {
        getRepoList()
    }

    val repoList : LiveData<List<Repo>>get() = repository.repoLive

    companion object {
        private const val DEFAULT_QUERY = "language:Kotlin"
    }

    fun getRepoList(){
        page++

        viewModelScope.launch(Dispatchers.IO) {
            repository.getRepoList(DEFAULT_QUERY,page,itemPerPage)
        }
    }
}