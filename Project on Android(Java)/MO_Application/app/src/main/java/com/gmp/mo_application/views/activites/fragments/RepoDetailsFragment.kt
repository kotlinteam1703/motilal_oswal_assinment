package com.gmp.mo_application.views.activites.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.gmp.mo_application.R
import java.text.SimpleDateFormat
import java.util.*
import com.gmp.mo_application.databinding.FragmentRepoDetailsBinding as FragmentRepoDetailsBinding


class RepoDetailsFragment : Fragment(R.layout.fragment_repo_details) {
    private var _binding: FragmentRepoDetailsBinding? = null
    private val binding get() = _binding!!
    private val args: RepoDetailsFragmentArgs by navArgs()
    private val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        _binding = FragmentRepoDetailsBinding.bind(view)
        binding.apply {
            name.text = args.repo.name
            username.text = args.repo.owner.login
            language.text = args.repo.language
            description.text = args.repo.description


            Glide.with(view)
                .load(args.repo.owner.avatar_url)
                .error(android.R.drawable.stat_notify_error)
                .into(avatar)


            stars.text = args.repo.stars.toString()
            forks.text = args.repo.forks.toString()
            watchers.text = args.repo.watchers.toString()
            issuesOpened.text = args.repo.openIssues.toString()
            createDate.text = formatDate(args.repo.createDate)
            updateDate.text = formatDate(args.repo.updateDate)


        }

    }

    fun formatDate(inputDate: String): String {
        return try {
            val inputDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.US)
            val outputDateFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)

            val parsedDate = inputDateFormat.parse(inputDate) ?: return inputDate
            outputDateFormat.format(parsedDate)

        } catch (ex: Exception) {
            Log.e("formatDate", "failed to format date")
            inputDate
        }
    }


}