package com.gmp.mo_application.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gmp.mo_application.repositorys.Repo_repository

class RepoListViewModelFactory(val repository : Repo_repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       return RepoListViewModel(repository )as T
    }
}