package com.example.myserviceapplicatonforjava;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.Random;

public class MyService extends Service {
    private static final String TAG = "MyService";

    private int mRandomNumber;
    private boolean mIsRandomNumberGeneretorOn;
    private final int MIN = 0;
    private final int MAX =100;
    private IBinder mBinder = new MyServiceBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Service is Started", Toast.LENGTH_SHORT).show();
        System.out.println("#@ MyService Thread id : "+Thread.currentThread().getId());
        mIsRandomNumberGeneretorOn=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                startRandomNoGenerator();
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "Service is stop", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "onDestroy: Service is stop explicitly");
        stopRandomNoGenerator();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void startRandomNoGenerator(){
        while (mIsRandomNumberGeneretorOn){
            try {
                Thread.sleep(1000);
                if(mIsRandomNumberGeneretorOn){
                    mRandomNumber =new Random().nextInt(MAX)+MIN;
                    Log.i("Myservide", "startRandomNoGenerator: Thread "+Thread.currentThread().getId()+ " Random number : "+mRandomNumber);
                }

            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public int getRandomNo(){
        return mRandomNumber;
    }

    private void stopRandomNoGenerator(){
        mIsRandomNumberGeneretorOn=false;
    }

    class MyServiceBinder extends Binder{
        /*
        * Binder is the abstract class which implements the IBinder interface
        * */

        public MyService getService(){
            return MyService.this;
        }
    }




}
