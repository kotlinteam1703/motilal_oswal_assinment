package com.example.myserviceapplicatonforjava;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

public class MyJobService extends JobService {

    private static final String TAG = "MyJobService";

    private int mRandomNumber;
    private boolean mIsRandomNumberGeneretorOn;
    private final int MIN = 0;
    private final int MAX = 100;
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "onStartJob: is working fine");
        doInBackground();
        return true;
    }

    private void doInBackground() {
        Toast.makeText(getApplicationContext(), "Service is Started", Toast.LENGTH_SHORT).show();
        System.out.println("#@ MyJobService Thread id : "+Thread.currentThread().getId());
        mIsRandomNumberGeneretorOn=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                startRandomNoGenerator();
            }
        }).start();

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(TAG, "onStopJob: is Executed");
        return true;
    }
    private void startRandomNoGenerator(){
        while (mIsRandomNumberGeneretorOn){
            try {
                Thread.sleep(1000);
                if(mIsRandomNumberGeneretorOn){
                    mRandomNumber =new Random().nextInt(MAX)+MIN;
                    Log.i("Myservide", "startRandomNoGenerator: Thread "+Thread.currentThread().getId()+ " Random number : "+mRandomNumber);
                }

            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
